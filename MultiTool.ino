#include "src/Fonts/mt_crystal.h"
#include "src/Fonts/mt_segoe.h"

#include "src/Peripherials/mt_peripherials.h"
#include "src/Modules/mt_menuModule.h"

#include "src/Models/mt_ModuleStackItem.h"
#include "src/Models/mt_ModuleRunResult.h"

using namespace Multitool;

// Globals --------------------------------------------------------------------

Peripherials peripherials;

ModuleStackItem * currentModule = nullptr;

// Setup ----------------------------------------------------------------------

void setup() {

  peripherials.init();
}

// Loop -----------------------------------------------------------------------

void loop() 
{
  if (currentModule == nullptr) 
  {
    MenuModule * menuModule = new MenuModule(peripherials);

    currentModule = new ModuleStackItem();
    currentModule->prev = nullptr;
    currentModule->next = nullptr;
    currentModule->module = menuModule;
  }

  ModuleRunResult result = currentModule->module->run();

  if (result.action == moduleRunActionAdd) 
  {
    ModuleStackItem * nextModule = new ModuleStackItem();
    nextModule->prev = currentModule;
    currentModule->next = nextModule;
    nextModule->next = nullptr;
    nextModule->module = result.module;

    currentModule = nextModule;
  } 
  else if (result.action == moduleRunActionClose) 
  {
    ModuleStackItem * closed = currentModule;
    currentModule = currentModule->prev;

    if (currentModule != nullptr)
      currentModule->next = nullptr;

    delete closed->module;
    delete closed;
  }
}
