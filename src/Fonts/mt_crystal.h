#ifndef __MT_CRYSTAL_H__
#define __MT_CRYSTAL_H__

#include <ILI9341_t3.h>

namespace Multitool
{
    #ifdef __cplusplus
    extern "C" {
    #endif

    extern const ILI9341_t3_font_t Crystal30;
    extern const ILI9341_t3_font_t Crystal35;
    extern const ILI9341_t3_font_t Crystal40;
    extern const ILI9341_t3_font_t Crystal45;
    extern const ILI9341_t3_font_t Crystal50;
    extern const ILI9341_t3_font_t Crystal60;
    extern const ILI9341_t3_font_t Crystal70;
    extern const ILI9341_t3_font_t Crystal75;
    extern const ILI9341_t3_font_t Crystal80;
    extern const ILI9341_t3_font_t Crystal90;

    #ifdef __cplusplus
    } // extern "C"
    #endif
}

#endif
