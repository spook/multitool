#ifndef __SegoeUi9_h__
#define __SegoeUi9_h__

#include <ILI9341_t3.h>

#ifdef __cplusplus
extern "C" {
#endif

extern const ILI9341_t3_font_t SegoeUi9;
extern const ILI9341_t3_font_t SegoeUi9Bold;
extern const ILI9341_t3_font_t SegoeUi11;
extern const ILI9341_t3_font_t SegoeUi13;

#ifdef __cplusplus
} // extern "C"
#endif

#endif
