#include "mt_ModuleDesc.h"

namespace Multitool 
{
    ModuleDesc::ModuleDesc(const char * title,
            bool showActiveAreas,
            TouchArea * touchAreas,
            int touchAreaCount) 
    {
        this->title = title;
        this->showActiveAreas = showActiveAreas;
        this->touchAreas = touchAreas;
        this->touchAreaCount = touchAreaCount;
    }

    ModuleDesc::~ModuleDesc() 
    {

    }

    const char * ModuleDesc::getTitle()
    {
        return title;
    }

    bool ModuleDesc::getShowActiveAreas() 
    {
        return showActiveAreas;
    }

    int ModuleDesc::getTouchAreaCount() 
    {
        return touchAreaCount;
    }

    TouchArea * ModuleDesc::getTouchAreas() 
    {
        return touchAreas;
    }
}