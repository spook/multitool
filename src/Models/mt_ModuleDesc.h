#ifndef __MT_MODULE_DESC_H__
#define __MT_MODULE_DESC_H__

#include "mt_TouchArea.h"

namespace Multitool
{
    struct ModuleDesc 
    {
    private:
        const char * title;
        bool showActiveAreas;
        TouchArea * touchAreas;
        int touchAreaCount;

    public: 
        ModuleDesc(const char * title,
            bool showActiveAreas,
            TouchArea * touchAreas,
            int touchAreaCount);
        ~ModuleDesc();

        const char * getTitle();
        bool getShowActiveAreas();
        int getTouchAreaCount();
        TouchArea * getTouchAreas();
    };
}

#endif