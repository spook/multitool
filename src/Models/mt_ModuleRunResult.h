#ifndef __MT_MODULE_RUN_RESULT_H__
#define __MT_MODULE_RUN_RESULT_H__

namespace Multitool
{
    enum ModuleRunAction
    {
        moduleRunActionClose,
        moduleRunActionAdd
    };

    class BaseModule;

    struct ModuleRunResult
    {
    public:
        BaseModule * module;
        ModuleRunAction action;
    };
}

#endif