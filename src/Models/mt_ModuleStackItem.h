#ifndef __MT_MODULE_STACK_ITEM_H__
#define __MT_MODULE_STACK_ITEM_H__

#include "../Modules/mt_baseModule.h"

namespace Multitool
{
    struct ModuleStackItem;

    struct ModuleStackItem
    {
        ModuleStackItem * prev;
        ModuleStackItem * next;
        BaseModule * module;
    };
}

#endif