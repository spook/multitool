#include "mt_TouchArea.h"

namespace Multitool
{    
    bool TouchArea::contains(int x, int y) 
    {
        return (x >= this->x && x < this->x + this->width &&
            y >= this->y && y < this->y + this->height);
    }
}