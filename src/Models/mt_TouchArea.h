#ifndef __MT_TOUCH_AREA_H__
#define __MT_TOUCH_AREA_H__

namespace Multitool 
{
    class TouchArea 
    {
    public:
        int x;
        int y;
        int width;
        int height;
        int index;

        bool contains(int x, int y);
    };
}

#endif