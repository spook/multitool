#include "mt_baseModule.h"
#include "../Fonts/mt_segoe.h"

namespace Multitool
{
    // Private methods ------------------------------------------------------------

    void BaseModule::paintHeader()
    {
        peripherials.screenBuffer.fillRect(0, 0, 319, HEADER_HEIGHT, COLOR(85, 200, 255));
        peripherials.screenBuffer.setFont(SegoeUi9Bold);
        peripherials.screenBuffer.setCursor(3, 3);
        peripherials.screenBuffer.setTextColor(COLOR(0, 0, 0));
        peripherials.screenBuffer.print(getModuleDesc()->getTitle());

        char * time = new char[6];

        int h = hour();
        int m = minute();

        time[0] = h / 10 % 10 + 48;
        time[1] = h % 10 + 48;
        time[2] = ':';
        time[3] = m / 10 % 10 + 48;
        time[4] = m % 10 + 48;
        time[5] = 0;

        peripherials.screenBuffer.setCursor(280, 3);
        peripherials.screenBuffer.print(time);

        delete time;
    }

    // Protected methods ----------------------------------------------------------

    void BaseModule::paintActiveAreas()
    {

    }

    void BaseModule::paint() 
    {
        if (!isScreenSaverMode && peripherials.touchscreen.getLastTouchEventMs() > SCREENSAVER_TIME_MS) 
        {
            isScreenSaverMode = true;
        }

        peripherials.screenBuffer.clear();

        if (!isScreenSaverMode)
        {
            paintModule();
            paintHeader();

            if (isActiveAreasMode) 
            {
                paintActiveAreas();
            }
        }

        peripherials.screenBuffer.paintTo(&(peripherials.display));
    }

    int BaseModule::processTouch() 
    {
        if (activeAreaTime > ACTIVE_AREAS_TIME_MS) 
        {
            isActiveAreasMode = false;
        }

        if (peripherials.touchscreen.isTouch()) 
        {
            bool moduleSupportsActiveAreas = getModuleDesc()->getShowActiveAreas();

            if (isScreenSaverMode)
            {
                // Disable screen saver, ignore touch
                isScreenSaverMode = false;
            }
            else if (moduleSupportsActiveAreas && !isActiveAreasMode)
            {
                isActiveAreasMode = true;
                activeAreaTime = 0;
            }
            else
            {
                int x, y;
                peripherials.touchscreen.getTouchPoint(x, y);
                
                for (int i = 0; i < getModuleDesc()->getTouchAreaCount(); i++) 
                {
                    auto & area = getModuleDesc()->getTouchAreas()[i];

                    if (area.contains(x, y)) 
                    {
                        isActiveAreasMode = false;
                        return area.index;
                    }
                }            
            }
        }

        return -1;
    }

    // Public methods -------------------------------------------------------------

    BaseModule::BaseModule(Peripherials & newPeripherials)
        : peripherials(newPeripherials)
    {                
        isScreenSaverMode = false;
        isActiveAreasMode = false;
    }

    BaseModule::~BaseModule()
    {

    }
}