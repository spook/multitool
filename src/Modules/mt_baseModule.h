#ifndef __MT_BASE_MODULE_H__
#define __MT_BASE_MODULE_H__

#include "../Peripherials/mt_peripherials.h"
#include "../Tools/mt_screenBuffer.h"
#include "../Models/mt_ModuleRunResult.h"
#include "../Models/mt_ModuleDesc.h"

#include <TimeLib.h>

namespace Multitool 
{
    class BaseModule
    {
    private:
        void paintHeader();

    protected:
        const unsigned long SCREENSAVER_TIME_MS = 30000; // 30s
        const unsigned long ACTIVE_AREAS_TIME_MS = 5000; // 5s

        const int HEADER_HEIGHT = 15;
        const int CONTENT_MARGIN = 5;

        Peripherials & peripherials;

        bool isScreenSaverMode;
        bool isActiveAreasMode;
        elapsedMillis activeAreaTime;

        virtual ModuleDesc * getModuleDesc() = 0;
        virtual void paintModule() = 0;
        virtual void paintActiveAreas();
        
        void paint();
        int processTouch();

    public:
        BaseModule(Peripherials & newPeripherials);
        virtual ~BaseModule();

        virtual ModuleRunResult run() = 0;
    };
}

#endif