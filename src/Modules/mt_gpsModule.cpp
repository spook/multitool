#include "mt_gpsModule.h"

#include "../Tools/mt_uiPainter.h"
#include "../Fonts/mt_crystal.h"

namespace Multitool
{
    // Private methods --------------------------------------------------------

    void GpsModule::paintSpeed(float speed, bool valid) 
    {
        int panelX = 0;
        int panelY = HEADER_HEIGHT + CONTENT_MARGIN;
        int panelWidth = (320 - CONTENT_MARGIN) * 2 / 3;
        int panelHeight = (240 - 2 * CONTENT_MARGIN) / 2;

        if (!valid) 
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, "---.-", panelX + 10, panelY + 25, Crystal50);
        }
        else
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, speed, panelX + 10, panelY + 25, Crystal50);
        }

        peripherials.screenBuffer.setFont(SegoeUi11);
        peripherials.screenBuffer.print("  km/h");

        UiPainter::paintPanel(peripherials.screenBuffer,
            panelX, 
            panelY, 
            panelWidth, 
            panelHeight,
            "Speed",
            50);

        // Bars

        if (speed > maxSpeed)
            maxSpeed = speed;

        float percentage = speed / maxSpeed;

        // x * SPEED_BAR_WIDTH + (x - 1) * SPEED_BAR_MARGIN = speed_bar_area
        // x * SPEED_BAR_WIDTH + x * SPEED_BAR_MARGIN - SPEED_BAR_MARGIN = speed_bar_area
        // x = (speed_bar_area + SPEED_BAR_MARGIN) / (SPEED_BAR_WIDTH + SPEED_BAR_MARGIN)

        int barCount = ((panelWidth - 2 * CONTENT_MARGIN) + SPEED_BAR_MARGIN) / (SPEED_BAR_WIDTH + SPEED_BAR_MARGIN);
        int barAreaWidth = barCount * SPEED_BAR_WIDTH + (barCount - 1) * SPEED_BAR_MARGIN;        

        int barX = panelX + (panelWidth - barAreaWidth) / 2;
        int barY = panelY + panelHeight - CONTENT_MARGIN - SPEED_BAR_HEIGHT - 1;
        for (int i = 0; i < barCount; i++) 
        {
            uint16_t color = (valid && ((float)i / (barCount - 1) <= (percentage)) ? COLOR(0, 255, 255) : COLOR(128, 128, 128));

            peripherials.screenBuffer.fillRect(barX, barY, barX + SPEED_BAR_WIDTH - 1, barY + SPEED_BAR_HEIGHT - 1, color);

            barX += SPEED_BAR_WIDTH + SPEED_BAR_MARGIN;
        }
    }

    void GpsModule::paintLocation(float lon, float lat, bool valid)
    {
        int panelX = 0;
        int panelY = HEADER_HEIGHT + CONTENT_MARGIN + (240 - 2 * CONTENT_MARGIN) / 2 + CONTENT_MARGIN;
        int panelWidth = (320 - CONTENT_MARGIN) * 2 / 3;
        int panelHeight = (240 - 2 * CONTENT_MARGIN) / 2;

        // Latitude

        peripherials.screenBuffer.setTextColor(COLOR(255, 255, 255));
        peripherials.screenBuffer.setFont(SegoeUi11);
        peripherials.screenBuffer.setCursor(panelX + 10, panelY + 20);
        peripherials.screenBuffer.print("Lat ");

        if (!valid) 
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, "---.-", panelX + 40, panelY + 20, Crystal30);
        }
        else
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, lat, 4, panelX + 40, panelY + 20, Crystal30);
        }

        peripherials.screenBuffer.setFont(SegoeUi11);
        peripherials.screenBuffer.print("  o");

        // Longitude

        peripherials.screenBuffer.setFont(SegoeUi11);
        peripherials.screenBuffer.setCursor(panelX + 10, panelY + 60);
        peripherials.screenBuffer.print("Lon ");

        if (!valid) 
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, "---.-", panelX + 40, panelY + 60, Crystal30);
        }
        else
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, lon, 4, panelX + 40, panelY + 60, Crystal30);
        }

        peripherials.screenBuffer.setFont(SegoeUi11);
        peripherials.screenBuffer.print("  o");

        UiPainter::paintPanel(peripherials.screenBuffer,
            panelX, 
            panelY, 
            panelWidth, 
            panelHeight,
            "Location",
            50);
    }

    void GpsModule::paintSatellites(int satellites, bool valid)
    {
        int panelX = (320 - CONTENT_MARGIN) * 2 / 3 + CONTENT_MARGIN;
        int panelY = HEADER_HEIGHT + CONTENT_MARGIN;
        int panelWidth = (320 - CONTENT_MARGIN) * 1 / 3;
        int panelHeight = (240 - 3 * CONTENT_MARGIN) / 3;

        if (!valid)
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, "--", panelX + 10, panelY + 25, Crystal30);    
        }
        else
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, satellites, panelX + 10, panelY + 25, Crystal30);    
        }        
        
        UiPainter::paintPanel(peripherials.screenBuffer,
            panelX, 
            panelY, 
            panelWidth, 
            panelHeight,
            "Sat",
            30);
    }

    void GpsModule::paintHeading(float heading, bool valid)
    {
        int panelX = (320 - CONTENT_MARGIN) * 2 / 3 + CONTENT_MARGIN;
        int panelY = HEADER_HEIGHT + CONTENT_MARGIN + (240 - 3 * CONTENT_MARGIN) / 3 + CONTENT_MARGIN;
        int panelWidth = (320 - CONTENT_MARGIN) * 1 / 3;
        int panelHeight = (240 - 3 * CONTENT_MARGIN) / 3;
       
        if (!valid)
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, "---", panelX + 10, panelY + 25, Crystal30);    
            peripherials.screenBuffer.setFont(SegoeUi11);
            peripherials.screenBuffer.print("  o");
        }
        else
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, (int)heading, panelX + 10, panelY + 25, Crystal30);    
            peripherials.screenBuffer.setFont(SegoeUi11);
            peripherials.screenBuffer.print("  o");
        }        

        UiPainter::paintPanel(peripherials.screenBuffer,
            panelX, 
            panelY, 
            panelWidth, 
            panelHeight,
            "Hdg",
            30);
    }

    void GpsModule::paintAltitude(float altitude, bool altitudeValid)
    {
        int panelX = (320 - CONTENT_MARGIN) * 2 / 3 + CONTENT_MARGIN;
        int panelY = HEADER_HEIGHT + CONTENT_MARGIN + (240 - 3 * CONTENT_MARGIN) / 3 * 2 + CONTENT_MARGIN;
        int panelWidth = (320 - CONTENT_MARGIN) * 1 / 3;
        int panelHeight = (240 - 3 * CONTENT_MARGIN) / 3;
        
        if (!altitudeValid)
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, "----", panelX + 10, panelY + 25, Crystal30);    
        }
        else
        {
            UiPainter::paintShadowedText(peripherials.screenBuffer, (int)altitude, panelX + 10, panelY + 25, Crystal30);    
        }        
        
        peripherials.screenBuffer.setFont(SegoeUi11);
        peripherials.screenBuffer.print("  m");

        UiPainter::paintPanel(peripherials.screenBuffer,
            panelX, 
            panelY, 
            panelWidth, 
            panelHeight,
            "Alt",
            30);    
    }

    // Protected methods ------------------------------------------------------

    ModuleDesc * GpsModule::getModuleDesc()
    {
        return moduleDesc;
    }

    void GpsModule::paintModule()
    {
        // Speed

        float speed = peripherials.gps.getSpeed();
        bool speedValid = speed != TinyGPS::GPS_INVALID_F_SPEED;

        paintSpeed(speed, speedValid);

        // Location

        float lat, lon;
        peripherials.gps.getLocation(lat, lon);
        bool locationValid = lat != TinyGPS::GPS_INVALID_F_ANGLE && lon != TinyGPS::GPS_INVALID_F_ANGLE;

        paintLocation(lon, lat, locationValid);

        // Satellites

        int satellites =  peripherials.gps.getSatellites();
        bool satellitesValid = satellites != TinyGPS::GPS_INVALID_SATELLITES;

        paintSatellites(satellites, satellitesValid);

        // Heading

        float heading = peripherials.gps.getHeading();
        bool headingValid = heading != TinyGPS::GPS_INVALID_F_ANGLE;

        paintHeading(heading, headingValid);

        // Altitude

        float altitude = peripherials.gps.getAltitude();
        bool altitudeValid = altitude != TinyGPS::GPS_INVALID_F_ALTITUDE;

        paintAltitude(altitude, altitudeValid);
    }

    // Public methods ---------------------------------------------------------

    GpsModule::GpsModule(Peripherials & peripherials)
        : BaseModule(peripherials)
    {
        moduleDesc = new ModuleDesc("GPS data",
            false,
            nullptr,
            0);

        maxSpeed = 1.0f;
    }

    GpsModule::~GpsModule()
    {
        delete moduleDesc;
    }

    ModuleRunResult GpsModule::run() 
    {
        while (true)
        {
            paint();
            peripherials.gps.update();
            processTouch();
        }
    }
}