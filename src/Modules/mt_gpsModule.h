#ifndef __MT_GPS_MODULE_H__
#define __MT_GPS_MODULE_H__

#include "mt_baseModule.h"

namespace Multitool
{
    class GpsModule : public BaseModule
    {
    private:
        const int SPEED_BAR_WIDTH = 7;
        const int SPEED_BAR_MARGIN = 3;
        const int SPEED_BAR_HEIGHT = 12;

        ModuleDesc * moduleDesc;

        float maxSpeed;

        void paintSpeed(float speed, bool valid);
        void paintLocation(float lon, float lat, bool valid);
        void paintSatellites(int satellites, bool valid);
        void paintHeading(float heading, bool valid);
        void paintAltitude(float altitude, bool altitudeValid);

    protected:
        ModuleDesc * getModuleDesc();
        void paintModule();

    public:
        GpsModule(Peripherials & peripherials);
        ~GpsModule();

        ModuleRunResult run();
    };
}

#endif