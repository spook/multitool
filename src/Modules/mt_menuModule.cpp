#include "mt_menuModule.h"
#include "../Tools/mt_uiPainter.h"
#include "../Modules/mt_weatherModule.h"
#include "../Modules/mt_gpsModule.h"
#include "../Models/mt_ModuleRunResult.h"
#include "../Models/mt_ModuleDesc.h"
#include "../Fonts/mt_segoe.h"

namespace Multitool
{
    // Private methods --------------------------------------------------------

    // Protected methods ------------------------------------------------------

    void MenuModule::paintModule()
    {
        // ENV button

        auto & envArea = moduleDesc->getTouchAreas()[0];

        UiPainter::paintMainButton(peripherials.screenBuffer,
            envArea.x,
            envArea.y,
            envArea.width,
            envArea.height);

        peripherials.screenBuffer.setFont(SegoeUi13);
        peripherials.screenBuffer.setTextColor(COLOR(255, 255, 255));
        peripherials.screenBuffer.setCursor(envArea.x + 35, envArea.y + 30);
        peripherials.screenBuffer.print("ENV");

        peripherials.screenBuffer.setFont(SegoeUi9);
        peripherials.screenBuffer.setTextColor(COLOR(192, 192, 192));
        peripherials.screenBuffer.setCursor(envArea.x + 15, envArea.y + 60);
        peripherials.screenBuffer.print("Environment");
    
        peripherials.screenBuffer.setCursor(envArea.x + 30, envArea.y + 75);
        peripherials.screenBuffer.print("sensor");

        // GPS button
        
        auto & gpsArea = moduleDesc->getTouchAreas()[1];

        UiPainter::paintMainButton(peripherials.screenBuffer,
            gpsArea.x,
            gpsArea.y,
            gpsArea.width,
            gpsArea.height);

        peripherials.screenBuffer.setFont(SegoeUi13);
        peripherials.screenBuffer.setTextColor(COLOR(255, 255, 255));
        peripherials.screenBuffer.setCursor(gpsArea.x + 35, gpsArea.y + 30);
        peripherials.screenBuffer.print("GPS");

        peripherials.screenBuffer.setFont(SegoeUi9);
        peripherials.screenBuffer.setTextColor(COLOR(192, 192, 192));
        peripherials.screenBuffer.setCursor(gpsArea.x + 25, envArea.y + 60);
        peripherials.screenBuffer.print("GPS data");
    }

    // Public methods ---------------------------------------------------------

    MenuModule::MenuModule(Peripherials & peripherials)
            : BaseModule(peripherials)
    {
        TouchArea * areas = new TouchArea[2];
        areas[0].x = 0;
        areas[0].y = HEADER_HEIGHT + CONTENT_MARGIN;
        areas[0].width = BUTTON_WIDTH;
        areas[0].height = BUTTON_HEIGHT;
        areas[0].index = 1;        

        areas[1].x = BUTTON_WIDTH + CONTENT_MARGIN;
        areas[1].y = HEADER_HEIGHT + CONTENT_MARGIN;
        areas[1].width = BUTTON_WIDTH;
        areas[1].height = BUTTON_HEIGHT;
        areas[1].index = 2;

        moduleDesc = new ModuleDesc("Spooksoft Multitool v1.0",
            false,
            areas,
            2);
    }

    MenuModule::~MenuModule() 
    {
        delete moduleDesc;
    }

    ModuleDesc * MenuModule::getModuleDesc() 
    {
        return moduleDesc;
    }

    ModuleRunResult MenuModule::run() 
    {
        while (true) 
        {
            paint();

            for (int i = 0; i < 10; i++) 
            {
                int result = processTouch();
                
                if (result == 1) 
                {
                    // Open weather module

                    ModuleRunResult result;
                    result.module = new WeatherModule(peripherials);
                    result.action = moduleRunActionAdd;
                    return result;
                }
                else if (result == 2)
                {
                    // Open GPS module

                    ModuleRunResult result;
                    result.module = new GpsModule(peripherials);
                    result.action = moduleRunActionAdd;
                    return result;
                }

                delay(100);
            }
        }
    }
}