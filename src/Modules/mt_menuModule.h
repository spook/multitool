#ifndef __MT_MENU_MODULE_H__
#define __MT_MENU_MODULE_H__

#include "mt_baseModule.h"
#include "../Tools/mt_screenBuffer.h"
#include "../Peripherials/mt_display.h"
#include "../Peripherials/mt_touchscreen.h"
#include "../Peripherials/mt_envSensor.h"
#include "../Models/mt_ModuleRunResult.h"

namespace Multitool 
{
    class MenuModule : public BaseModule
    {
    private:
        const int BUTTON_HEIGHT = (240 - HEADER_HEIGHT - 3 * CONTENT_MARGIN) / 2;
        const int BUTTON_WIDTH = (320 - 2 * CONTENT_MARGIN) / 3;

        ModuleDesc * moduleDesc;

    protected:
        void paintModule();

    public:
        MenuModule(Peripherials & peripherials);
        ~MenuModule();

        ModuleDesc * getModuleDesc();

        ModuleRunResult run();
    };
}

#endif