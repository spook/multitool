#include "mt_weatherModule.h"
#include "../Fonts/mt_crystal.h"
#include "../Fonts/mt_segoe.h"
#include "../Tools/mt_uiPainter.h"

namespace Multitool
{
    // Private methods ------------------------------------------------------------

    void WeatherModule::updateValues() 
    {
        peripherials.envSensor.takeForcedMeasurement();
        temperature = peripherials.envSensor.readTemperature();
        humidity = peripherials.envSensor.readHumidity();
        pressure = peripherials.envSensor.readPressure() / 100;
    }

    void WeatherModule::updateIfNeeded(bool & alreadyUpdated)
    {
        if (alreadyUpdated)
            return;

        updateValues();

        alreadyUpdated = true;
    }

    void WeatherModule::updateTemp(DataSeries * dataSeries, bool & valuesUpdated)
    {
        if (dataSeries->isTimerElapsed()) 
        {
            updateIfNeeded(valuesUpdated);
            dataSeries->resetTimer();
            dataSeries->pushEntry(temperature);
        }
    }

    void WeatherModule::updateHum(DataSeries * dataSeries, bool & valuesUpdated)
    {
        if (dataSeries->isTimerElapsed()) 
        {
            updateIfNeeded(valuesUpdated);
            dataSeries->resetTimer();
            dataSeries->pushEntry(humidity);
        }
    }

    void WeatherModule::updatePressure(DataSeries * dataSeries, bool & valuesUpdated)
    {
        if (dataSeries->isTimerElapsed()) 
        {
            updateIfNeeded(valuesUpdated);
            dataSeries->resetTimer();
            dataSeries->pushEntry(pressure);
        }
    }

    void WeatherModule::updateData() 
    {
        bool dataUpdated = false;

        updateTemp(tempData1Min, dataUpdated);
        updateTemp(tempData1Hour, dataUpdated);
        updateTemp(tempData8Hours, dataUpdated);
        updateTemp(tempData24Hours, dataUpdated);

        updateHum(humData1Min, dataUpdated);
        updateHum(humData1Hour, dataUpdated);
        updateHum(humData8Hours, dataUpdated);
        updateHum(humData24Hours, dataUpdated);

        updatePressure(pressureData1Min, dataUpdated);
        updatePressure(pressureData1Hour, dataUpdated);
        updatePressure(pressureData8Hours, dataUpdated);
        updatePressure(pressureData24Hours, dataUpdated);
    }

    void WeatherModule::paintBlock(const char * title,
        int titleWidth,
        int panelStart,
        DataSeries * shortData,
        DataSeries * longData,
        float currentValue,
        const char * suffix,
        uint16_t longChartLineColor,
        uint16_t longChartFillColor,
        uint16_t shortChartLineColor) 
    {
        // Temperature

        UiPainter::paintDoubleCharts(&(peripherials.screenBuffer),
            shortData,
            longData,
            CHART_X,
            panelStart + 3,
            CHART_WIDTH,
            PANEL_HEIGHT - 6,
            shortChartLineColor,
            longChartLineColor,
            longChartFillColor);

        float minValue, maxValue;
        UiPainter::getChartsRanges(shortData, longData, minValue, maxValue);

        UiPainter::paintChartRanges(&(peripherials.screenBuffer),
            CHART_X,
            panelStart + 3,
            CHART_WIDTH,
            PANEL_HEIGHT - 6,
            minValue,
            maxValue);

        UiPainter::paintShadowedText(peripherials.screenBuffer, currentValue, 10, panelStart + 25, Crystal30);

        int curX, curY;
        peripherials.screenBuffer.getCursor(curX, curY);
        UiPainter::paintShadowedText(peripherials.screenBuffer, suffix, curX, curY, SegoeUi11);

        peripherials.screenBuffer.setCursor(0 + 3 + titleWidth + HEADER_HEIGHT, panelStart + 3);
        peripherials.screenBuffer.setTextColor(COLOR(255, 255, 255));
        peripherials.screenBuffer.setFont(SegoeUi9);
        peripherials.screenBuffer.print(longData->getPeriodDescription());

        UiPainter::paintPanel(peripherials.screenBuffer,
            0, 
            panelStart, 
            320, 
            PANEL_HEIGHT,
            title,
            titleWidth);
    }

    // Protected methods ----------------------------------------------------------

    ModuleDesc * WeatherModule::getModuleDesc()
    {
        return moduleDesc;
    }

    void WeatherModule::paintModule() 
    {
        // Temperature

        int panelStart = HEADER_HEIGHT + CONTENT_MARGIN;

        DataSeries * longChart;
        switch (longChartType) 
        {
            case 0: 
                longChart = tempData1Hour;
                break;
            case 1:
                longChart = tempData8Hours;
                break;
            case 2:
            default:
                longChart = tempData24Hours;
                break;
        }

        paintBlock("Temperature",
            80,
            panelStart,
            tempData1Min,
            longChart,
            temperature,
            " oC",
            COLOR(255, 0, 0),
            COLOR(128, 0, 0),
            COLOR(255, 128, 128));

        // Humidity

        panelStart = HEADER_HEIGHT + 2 * CONTENT_MARGIN + PANEL_HEIGHT;
        
        switch (longChartType) 
        {
            case 0: 
                longChart = humData1Hour;
                break;
            case 1:
                longChart = humData8Hours;
                break;
            case 2:
            default:
                longChart = humData24Hours;
                break;
        }

        paintBlock("Humidity",
            55,
            panelStart,
            humData1Min,
            longChart,            
            humidity,
            " %",
            COLOR(0, 255, 0),
            COLOR(0, 128, 0),
            COLOR(128, 255, 128));

        // Pressure

        panelStart = HEADER_HEIGHT + 3 * CONTENT_MARGIN + 2 * PANEL_HEIGHT;

        switch (longChartType) 
        {
            case 0: 
                longChart = pressureData1Hour;
                break;
            case 1:
                longChart = pressureData8Hours;
                break;
            case 2:
            default:
                longChart = pressureData24Hours;
                break;
        }

        paintBlock("Pressure",
            55,
            panelStart,
            pressureData1Min,
            longChart,
            pressure,
            " hPa",
            COLOR(0, 0, 255),
            COLOR(0, 0, 128),
            COLOR(128, 128, 255));
    }

    void WeatherModule::paintActiveAreas() 
    {
        for (int i = 0; i < moduleDesc->getTouchAreaCount(); i++) 
        {
            TouchArea & area = moduleDesc->getTouchAreas()[i];

            peripherials.screenBuffer.fillRect(area.x, area.y, area.x + area.width - 1, area.y + area.height - 1, COLOR(0, 0, 0));
            peripherials.screenBuffer.drawRect(area.x, area.y, area.x + area.width - 1, area.y + area.height - 1, COLOR(255, 255, 255));
        }        

        peripherials.screenBuffer.setFont(SegoeUi11);
        peripherials.screenBuffer.setTextColor(COLOR(255, 255, 255));

        peripherials.screenBuffer.setCursor(285, 25);
        peripherials.screenBuffer.print("X");

        peripherials.screenBuffer.setCursor(70, 25);
        peripherials.screenBuffer.print("Cycle period");
    }

    // Public methods -------------------------------------------------------------

    WeatherModule::WeatherModule(Peripherials & peripherials)
        : BaseModule(peripherials)
    {
        TouchArea * touchAreas = new TouchArea[2];

        touchAreas[0].x = 270;
        touchAreas[0].y = 10;
        touchAreas[0].width = 40;
        touchAreas[0].height = 40;
        touchAreas[0].index = 1;

        touchAreas[1].x = 60;
        touchAreas[1].y = 10;
        touchAreas[1].width = 200;
        touchAreas[1].height = 40;
        touchAreas[1].index = 2;

        moduleDesc = new ModuleDesc("Environment sensor",
            true,
            touchAreas,
            2);

        updateValues();

        tempData1Min = new DataSeries(5, CHART_WIDTH, temperature, 5 * 1000, false, "Current (5 sec)");
        tempData1Hour = new DataSeries(3, CHART_WIDTH, temperature, 60 * 60 * 1000, true, "1 hour");
        tempData8Hours = new DataSeries(3, CHART_WIDTH, temperature, 8 * 60 * 60 * 1000, true, "8 hours");
        tempData24Hours = new DataSeries(3, CHART_WIDTH, temperature, 24 * 60 * 60 * 1000, true, "24 hours");

        humData1Min = new DataSeries(5, CHART_WIDTH, humidity, 5 * 1000, false, "Current (5 sec)");
        humData1Hour = new DataSeries(3, CHART_WIDTH, humidity, 60 * 60 * 1000, true, "1 hour");
        humData8Hours = new DataSeries(3, CHART_WIDTH, humidity, 8 * 60 * 60 * 1000, true, "8 hours");
        humData24Hours = new DataSeries(3, CHART_WIDTH, humidity, 24 * 60 * 60 * 1000, true, "24 hours");

        pressureData1Min = new DataSeries(5, CHART_WIDTH, pressure, 5 * 1000, false, "Current (5 sec)");
        pressureData1Hour = new DataSeries(3, CHART_WIDTH, pressure, 60 * 60 * 1000, true, "1 hour");
        pressureData8Hours = new DataSeries(3, CHART_WIDTH, pressure, 8 * 60 * 60 * 1000, true, "8 hours");
        pressureData24Hours = new DataSeries(3, CHART_WIDTH, pressure, 24 * 60 * 60 * 1000, true, "24 hours");

        longChartType = 0;
    }

    WeatherModule::~WeatherModule()        
    {
        delete moduleDesc;

        delete tempData1Min;
        delete tempData1Hour;
        delete tempData8Hours;
        delete tempData24Hours;
        delete humData1Min;
        delete humData1Hour;
        delete humData8Hours;
        delete humData24Hours;
        delete pressureData1Min;
        delete pressureData1Hour;
        delete pressureData8Hours;
        delete pressureData24Hours;
    }

    ModuleRunResult WeatherModule::run()
    {
        while (1)
        {
            updateData();

            paint();
            
            bool exit = false;

            for (int i = 0; i < 10; i++) 
            {
                int result = processTouch();
                if (result == 1)
                {
                    exit = true;
                }
                else if (result == 2) 
                {
                    longChartType = (longChartType + 1) % 3;
                }

                delay(100);
            }

            if (exit)
                break;
        }

        ModuleRunResult result;
        result.action = moduleRunActionClose;
        return result;
    }
}