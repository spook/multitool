#ifndef __MT_WEATHERMODULE_H__
#define __MT_WEATHERMODULE_H__

#include "mt_baseModule.h"
#include "../Tools/mt_screenBuffer.h"
#include "../Tools/mt_dataSeries.h"
#include "../Peripherials/mt_display.h"
#include "../Peripherials/mt_envSensor.h"
#include "../Peripherials/mt_touchscreen.h"
#include "../Models/mt_ModuleRunResult.h"

namespace Multitool
{
    class WeatherModule : public BaseModule
    {
    private:
        const int PANEL_HEIGHT = (240 - HEADER_HEIGHT - 4 * CONTENT_MARGIN) / 3;

        const int CHART_X = 3;
        const int CHART_WIDTH = 313;

        ModuleDesc * moduleDesc;

        DataSeries * tempData1Min;
        DataSeries * tempData1Hour;
        DataSeries * tempData8Hours;
        DataSeries * tempData24Hours;

        DataSeries * humData1Min;
        DataSeries * humData1Hour;
        DataSeries * humData8Hours;
        DataSeries * humData24Hours;

        DataSeries * pressureData1Min;
        DataSeries * pressureData1Hour;
        DataSeries * pressureData8Hours;
        DataSeries * pressureData24Hours;

        int longChartType;

        float temperature;
        float humidity;
        float pressure;

        void updateValues();
        void updateIfNeeded(bool & alreadyUpdated);
        void updateTemp(DataSeries * dataSeries, bool & valuesUpdated);
        void updateHum(DataSeries * dataSeries, bool & valuesUpdated);
        void updatePressure(DataSeries * dataSeries, bool & valuesUpdated);
        void updateData();

        void paintBlock(const char * title,
            int titleWidth,
            int panelStart,
            DataSeries * shortData,
            DataSeries * longData,
            float currentValue,
            const char * suffix,
            uint16_t longChartLineColor,
            uint16_t longChartFillColor,
            uint16_t shortChartLineColor);

    protected:
        ModuleDesc * getModuleDesc();
        void paintModule();
        void paintActiveAreas();

    public:
        WeatherModule(Peripherials & peripherials);        
        ~WeatherModule();

        ModuleRunResult run();
    };
}

#endif