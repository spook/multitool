#include "mt_display.h"

namespace Multitool
{
    Display::Display()
        : ILI9341_t3(DISPLAY_CS_PIN, DISPLAY_DC_PIN, 255, DISPLAY_MOSI_PIN, DISPLAY_SCK_PIN, DISPLAY_MISO_PIN)
    {

    }

    void Display::init() 
    {
        begin();
        setRotation(1);
    }

    void Display::printFloat(float f, int decimalPlaces) {

        print((int)f);

        f -= (int)f;
        for (int i = 0; i < decimalPlaces; i++)
        f *= 10;

        print('.');

        print((int)f);
    }
}