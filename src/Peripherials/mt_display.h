#ifndef __MT_DISPLAY_H__
#define __MT_DISPLAY_H__

#include <ILI9341_t3.h>

#define DISPLAY_CS_PIN 10
#define DISPLAY_DC_PIN 9
#define DISPLAY_MOSI_PIN 11
#define DISPLAY_MISO_PIN 12
#define DISPLAY_SCK_PIN 13

#define COLOR(_r,_g,_b) ((((_r)&0xF8)<<8)|(((_g)&0xFC)<<3)|((_b)>>3))

namespace Multitool
{
    class Display : public ILI9341_t3
    {
    public:
        Display();
        void init();

        void printFloat(float f, int decimalPlaces);
    };
}

#endif