#include "mt_envSensor.h"

namespace Multitool
{
    // Public methods ---------------------------------------------------------

    EnvSensor::EnvSensor()
        : Adafruit_BME280()
    {
        
    }

    void EnvSensor::init()
    {
        begin(0x76, &Wire);
        
        setSampling(Adafruit_BME280::MODE_FORCED, 
                    Adafruit_BME280::SAMPLING_X8, // temperature 
                    Adafruit_BME280::SAMPLING_X8, // pressure 
                    Adafruit_BME280::SAMPLING_X8, // humidity 
                    Adafruit_BME280::FILTER_OFF,
                    Adafruit_BME280::STANDBY_MS_1000);
    }
}