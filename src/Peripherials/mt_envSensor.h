#ifndef __MT_ENVSENSOR_H__
#define __MT_ENVSENSOR_H__

#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>

namespace Multitool
{
    class EnvSensor : public Adafruit_BME280
    {
    public:
        EnvSensor();
        void init();
    };
}

#endif