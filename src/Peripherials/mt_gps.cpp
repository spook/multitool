#include "mt_gps.h"

namespace Multitool
{
    GPS::GPS()
        : TinyGPS()
    {
        
    }

    void GPS::init()
    {
        Serial5.begin(9600);
    }

    void GPS::update()
    {
        const char * firstSeq = "$GPRMC";
        const int firstSeqLen = 6;
        const char * lastSeq = "$GPGLL";
        const int lastSeqLen = 6;
        int index = 0;

        // 0 - waiting for first sequence
        // 1 - waiting for last sequence
        // 2 - waiting for (13) trailing the last sequence
        // 3 - finished
        int mode = 0;

        while (mode < 3)
        {
            while (Serial5.available())
            {
                int c = Serial5.read();

                if (mode == 0) 
                {
                    if (c == firstSeq[index])
                        index++;
                    else
                        index = 0;
                    
                    if (index == firstSeqLen) 
                    {
                        index = 0;
                        mode = 1;
                    }
                }
                else if (mode == 1)
                {
                    if (c == lastSeq[index])
                        index++;
                    else
                        index = 0;

                    if (index == lastSeqLen)
                    {
                        index = 0;
                        mode = 2;
                    }                
                }
                else if (mode == 2)
                {
                    if (c == 13) 
                    {
                        mode = 3;
                    }
                }

                encode(c);
            }
        }
    }

    int GPS::getSatellites() 
    {
        return satellites();
    }

    float GPS::getSpeed()
    {
        return f_speed_kmph();
    }

    float GPS::getHeading()
    {
        return f_course();
    }

    float GPS::getAltitude()
    {
        return f_altitude();
    }

    void GPS::getLocation(float & lat, float & lon)
    {       
        f_get_position(&lat, &lon);
    }
}