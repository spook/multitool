#ifndef __MT_GPS_H__
#define __MT_GPS_H__

#include <TinyGPS.h>

namespace Multitool
{
class GPS : private TinyGPS
{
    public:
        GPS();
        void init();
        void update();

        int getSatellites();
        float getSpeed();
        float getHeading();
        float getAltitude();
        void getLocation(float & lat, float & lon);
    };
}

#endif