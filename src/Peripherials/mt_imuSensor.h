#ifndef __MT_IMUSENSOR_H__
#define __MT_IMUSENSOR_H__

#include <Wire.h>
#include <MPU9250.h>

namespace Multitool
{
    class ImuSensor : public MPU9250
    {
    public:
        ImuSensor();
        void init();
    };
}

#endif