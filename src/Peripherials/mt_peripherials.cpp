#include "mt_peripherials.h"

namespace Multitool
{
    time_t getTeensy3Time()
    {
        return Teensy3Clock.get();
    }

    Peripherials::Peripherials() 
        : screenBuffer(320, 240, 0, 0)
    {

    }

    void Peripherials::paintBootProgress(int percentage, const char * module)
    {
        uint16_t progressFrameColor = COLOR(255, 255, 255);
        uint16_t progressBarColor = COLOR(0, 255, 0);

        int x1 = 100;
        int x2 = 220;
        int y1 = 117;
        int y2 = 123;

        int pbx1 = x1 + 2;
        int pbx2 = x2 - 2;
        int pby = (y1 + y2) / 2;
        int pbw = pbx2 - pbx1;

        screenBuffer.clear(0);
        screenBuffer.drawHLine(x1, x2, y1, progressFrameColor);
        screenBuffer.drawHLine(x1, x2, y2, progressFrameColor);
        screenBuffer.drawVLine(x1, y1, y2, progressFrameColor);
        screenBuffer.drawVLine(x2, y1, y2, progressFrameColor);

        screenBuffer.drawHLine(pbx1, pbx1 + pbw * percentage / 100, pby, progressBarColor);

        screenBuffer.setFont(SegoeUi9);
        screenBuffer.setTextColor(COLOR(255, 255, 255));

        screenBuffer.setCursor(x1, y1 - 18);
        screenBuffer.print("Booting...");

        screenBuffer.setCursor(x1, y2 + 10);
        screenBuffer.print(module);

        screenBuffer.paintTo(&display);
    }

    void Peripherials::init()
    {
        // Starting display management
        display.init();
        display.fillScreen(0);

        paintBootProgress(1 * 100 / 8, "Serial comms");

        // Starting serial port
        Serial.begin(9600);

        paintBootProgress(2 * 100 / 8, "GPS");

        // Initializing gps
        gps.init();

        paintBootProgress(3 * 100 / 8, "Touchscreen");

        // Starting touchscreen management
        touchscreen.init();

        paintBootProgress(4 * 100 / 8, "IMU sensor");

        // Starting IMU sensor
        imuSensor.init();

        paintBootProgress(5 * 100 / 8, "ENV sensor");

        // Starting weather sensor
        envSensor.init();

        paintBootProgress(6 * 100 / 8, "Mic initialization");

        // Setting up mic
        mic.init();

        // Setting up real time clock
        paintBootProgress(7 * 100 / 8, "RTC module");

        setSyncProvider(getTeensy3Time);

        paintBootProgress(8 * 100 / 8, "Initialized");

        delay(500);        
    }
}