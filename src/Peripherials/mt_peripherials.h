#ifndef __MT_PERIPHERIALS_H__
#define __MT_PERIPHERIALS_H__

#include <TimeLib.h>

#include "mt_display.h"
#include "mt_touchscreen.h"
#include "mt_envSensor.h"
#include "mt_gps.h"
#include "mt_mic.h"
#include "mt_imuSensor.h"
#include "../Tools/mt_screenBuffer.h"
#include "../Tools/mt_uiPainter.h"
#include "../Fonts/mt_segoe.h"

namespace Multitool 
{
    class BaseModule;

    struct Peripherials 
    {
    private:
        friend class BaseModule;

        Touchscreen touchscreen;
        Display display;

        void paintBootProgress(int percentage, const char * module);

    public:
        Peripherials();

        Mic mic;
        EnvSensor envSensor;
        ImuSensor imuSensor;
        GPS gps;
        ScreenBuffer screenBuffer;

        void init();
    };
}

#endif