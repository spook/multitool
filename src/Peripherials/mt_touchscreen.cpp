#include "mt_touchscreen.h"

namespace Multitool
{
    Touchscreen::Touchscreen()
        : XPT2046_Touchscreen(TOUCHSCREEN_CS_PIN, TOUCHSCREEN_IRQ_PIN)
    {
        tapFlag = false;

        lastTouchEventTime = 0;
    }

    void Touchscreen::init() 
    {
        begin();
        setRotation(1);
    }

    bool Touchscreen::isTouch() 
    {
        bool internalTouch = touched();

        if (!internalTouch)
            tapFlag = false;

        if (tapFlag)
            return false;

        if (internalTouch) 
        {
            lastTouchEventTime = 0;
            tapFlag = true;
            return true;
        }

        return false;
    }

    void Touchscreen::getTouchPoint(int & x, int & y) 
    {
        TS_Point logicPoint = getPoint();

        x = (logicPoint.x - 250) * 320 / (3800 - 250);
        y = (logicPoint.y - 250) * 240 / (3800 - 250);

        if (x < 0)
            x = 0;
        if (x > 319)
            x = 319;

        if (y < 0)
            y = 0;
        if (y > 239)
            y = 239;
    }

    unsigned long Touchscreen::getLastTouchEventMs()
    {
        return lastTouchEventTime;
    }
}