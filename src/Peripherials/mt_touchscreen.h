#ifndef __MT_TOUCHSCREEN_H__
#define __MT_TOUCHSCREEN_H__

#define TOUCHSCREEN_CS_PIN 8
#define TOUCHSCREEN_IRQ_PIN 23

#include <XPT2046_Touchscreen.h>

namespace Multitool
{
    class Touchscreen : protected XPT2046_Touchscreen
    {
    private:
        bool tapFlag;
        elapsedMillis lastTouchEventTime;

    public:
        Touchscreen();
        void init();

        bool isTouch();
        void getTouchPoint(int & x, int & y);
        unsigned long getLastTouchEventMs();
    };
}

#endif