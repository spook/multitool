#include "mt_dataSeries.h"

namespace Multitool 
{
    DataSeries::DataSeries(int bufferSize, 
        int dataSize, 
        float initialValue, 
        unsigned long dataCollectionPeriodMs,
        bool periodForWholeData,
        const char * periodDescription)
        : periodDescription(periodDescription)
    {
        this->bufferSize = bufferSize;
        buffer = new float[this->bufferSize];
        bufferIndex = 0;

        for (int i = 0; i < bufferSize; i++) 
        {
            buffer[i] = initialValue;
        }

        this->dataSize = dataSize;
        data = new float[this->dataSize];
        dataIndex = 0;

        for (int i = 0; i < dataSize; i++)
        {
            data[i] = initialValue;
        }

        timer = 0;

        if (periodForWholeData)
        {
            this->dataCollectionPeriodMs = dataCollectionPeriodMs / dataSize;
        }
        else
        {
            this->dataCollectionPeriodMs = dataCollectionPeriodMs;
        }        

        minValue = initialValue;
        maxValue = initialValue;
    }

    DataSeries::~DataSeries() 
    {
        delete[] buffer;
        delete[] data;
    }

    void DataSeries::resetTimer()
    {
        timer = 0;
    }

    bool DataSeries::isTimerElapsed() 
    {
        return timer >= dataCollectionPeriodMs;
    }

    void DataSeries::pushEntry(float value) 
    {
        // Store new value in buffer

        buffer[bufferIndex] = value;
        bufferIndex = (bufferIndex + 1) % bufferSize;

        // Evaluate average from buffer

        float sum = buffer[0];
        for (int i = 1; i < bufferSize; i++)
            sum += buffer[i];

        sum /= bufferSize;

        // Store average in data

        data[dataIndex] = sum;
        dataIndex = (dataIndex + 1) % dataSize;

        // Eval min/max

        minValue = data[0];
        maxValue = data[0];

        for (int i = 1; i < dataSize; i++) 
        {
            if (data[i] < minValue)
                minValue = data[i];
            if (data[i] > maxValue)
                maxValue = data[i];
        }

        Serial.print("After adding ");
        Serial.print(value);
        Serial.print(" -> ");
        Serial.print(minValue);
        Serial.print("/");
        Serial.println(maxValue);
    }

    int DataSeries::size() 
    {
        return dataSize;
    }

    float DataSeries::operator[] (int index) 
    {
        return data[(index + dataIndex) % dataSize];
    }

    float DataSeries::getMinValue()
    {
        return minValue;
    }

    float DataSeries::getMaxValue()
    {
        return maxValue;
    }

    const char * DataSeries::getPeriodDescription()
    {
        return periodDescription;
    }
}