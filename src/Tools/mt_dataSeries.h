#ifndef __MT_DATA_SERIES_H__
#define __MT_DATA_SERIES_H__

#include <Arduino.h>
#include <TimeLib.h>

namespace Multitool 
{
    class DataSeries
    {
    private:
        float * buffer;
        int bufferSize;
        int bufferIndex;
        float * data;
        int dataSize;
        int dataIndex;
        elapsedMillis timer;
        unsigned long dataCollectionPeriodMs;
        const char * periodDescription;

        float minValue;
        float maxValue;

    public:
        DataSeries(int bufferSize,
            int dataSize,
            float initialValue,
            unsigned long dataCollectionPeriodMs,
            bool periodForWholeData,
            const char * periodDescription);

        virtual ~DataSeries();

        void resetTimer();
        bool isTimerElapsed();
        void pushEntry(float value);
        int size();
        float getMinValue();
        float getMaxValue();
        const char * getPeriodDescription();

        float operator[](int index);
    };
}

#endif