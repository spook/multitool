#include "mt_screenBuffer.h"

namespace Multitool
{
    // Private methods --------------------------------------------------------

	void ScreenBuffer::drawFontBits(uint32_t bits, uint32_t numbits, uint32_t x, uint32_t y, uint32_t repeat)
	{
		//Serial.printf("      %d bits at %d,%d: %X\n", numbits, x, y, bits);

		if (bits == 0)
			return;

		do
		{
			uint16_t *pixel = this->buffer + bufferWidth * (y - bufferY) + x - bufferX;

			uint32_t x1 = x;
			uint32_t n = numbits;
			do
			{
				n--;
				if (bits & (1 << n))
				{
					*pixel = textcolor;
					//Serial.printf("        pixel at %d,%d\n", x1, y);
				}
				pixel++;
				x1++;
			} while (n > 0);
			y++;
			repeat--;
		} while (repeat);
	}

	void ScreenBuffer::drawFontChar(unsigned int c)
	{
		uint32_t bitoffset;
		const uint8_t *data;

		//Serial.printf("drawFontChar %d\n", c);

		if (c >= font->index1_first && c <= font->index1_last)
		{
			bitoffset = c - font->index1_first;
			bitoffset *= font->bits_index;
		}
		else if (c >= font->index2_first && c <= font->index2_last)
		{
			bitoffset = c - font->index2_first + font->index1_last - font->index1_first + 1;
			bitoffset *= font->bits_index;
		}
		else if (font->unicode)
		{
			return; // TODO: implement sparse unicode
		}
		else
		{
			return;
		}
		//Serial.printf("  index =  %d\n", fetchbits_unsigned(font->index, bitoffset, font->bits_index));
		data = font->data + fetchbits_unsigned(font->index, bitoffset, font->bits_index);

		uint32_t encoding = fetchbits_unsigned(data, 0, 3);
		if (encoding != 0)
			return;
		uint32_t fontWidth = fetchbits_unsigned(data, 3, font->bits_width);
		bitoffset = font->bits_width + 3;
		uint32_t fontHeight = fetchbits_unsigned(data, bitoffset, font->bits_height);
		bitoffset += font->bits_height;
		//Serial.printf("  size =   %d,%d\n", fontWidth, fontHeight);

		int32_t xoffset = fetchbits_signed(data, bitoffset, font->bits_xoffset);
		bitoffset += font->bits_xoffset;
		int32_t yoffset = fetchbits_signed(data, bitoffset, font->bits_yoffset);
		bitoffset += font->bits_yoffset;
		//Serial.printf("  offset = %d,%d\n", xoffset, yoffset);

		uint32_t delta = fetchbits_unsigned(data, bitoffset, font->bits_delta);
		bitoffset += font->bits_delta;
		//Serial.printf("  delta =  %d\n", delta);

		//Serial.printf("  cursor = %d,%d\n", cursor_x, cursor_y);

		// horizontally, we draw every pixel, or none at all
		if (cursor_x < bufferX)
			cursor_x = bufferX;

		int32_t origin_x = cursor_x + xoffset;
		if (origin_x < bufferX)
		{
			cursor_x -= xoffset;
			origin_x = bufferX;
		}
		if (origin_x + (int)fontWidth > bufferX + bufferWidth)
		{
			if (!wrap)
				return;

			origin_x = bufferX;
			if (xoffset >= bufferX)
			{
				cursor_x = bufferX;
			}
			else
			{
				cursor_x = bufferX - xoffset;
			}
			cursor_y += font->line_space;
		}
		if (cursor_y >= bufferHeight)
			return;
		cursor_x += delta;

		// vertically, the top and/or bottom can be clipped
		int32_t origin_y = cursor_y + font->cap_height - fontHeight - yoffset;
		//Serial.printf("  origin = %d,%d\n", origin_x, origin_y);

		// TODO: compute top skip and number of lines
		int32_t linecount = fontHeight;
		//uint32_t loopcount = 0;
		uint32_t y = origin_y;
		while (linecount)
		{
			//Serial.printf("    linecount = %d\n", linecount);
			uint32_t b = fetchbit(data, bitoffset++);
			if (b == 0)
			{
				//Serial.println("    single line");
				uint32_t x = 0;
				do
				{
					uint32_t xsize = fontWidth - x;
					if (xsize > 32)
						xsize = 32;
					uint32_t bits = fetchbits_unsigned(data, bitoffset, xsize);
					drawFontBits(bits, xsize, origin_x + x, y, 1);
					bitoffset += xsize;
					x += xsize;
				} while (x < fontWidth);
				y++;
				linecount--;
			}
			else
			{
				uint32_t n = fetchbits_unsigned(data, bitoffset, 3) + 2;
				bitoffset += 3;
				uint32_t x = 0;
				do
				{
					uint32_t xsize = fontWidth - x;
					if (xsize > 32)
						xsize = 32;
					//Serial.printf("    multi line %d\n", n);
					uint32_t bits = fetchbits_unsigned(data, bitoffset, xsize);
					drawFontBits(bits, xsize, origin_x + x, y, n);
					bitoffset += xsize;
					x += xsize;
				} while (x < fontWidth);
				y += n;
				linecount -= n;
			}
		}
	}

	uint32_t ScreenBuffer::fetchbit(const uint8_t *p, uint32_t index)
	{
		if (p[index >> 3] & (1 << (7 - (index & 7))))
			return 1;
		return 0;
	}

	uint32_t ScreenBuffer::fetchbits_unsigned(const uint8_t *p, uint32_t index, uint32_t required)
	{
		uint32_t val = 0;
		do
		{
			uint8_t b = p[index >> 3];
			uint32_t avail = 8 - (index & 7);
			if (avail <= required)
			{
				val <<= avail;
				val |= b & ((1 << avail) - 1);
				index += avail;
				required -= avail;
			}
			else
			{
				b >>= avail - required;
				val <<= required;
				val |= b & ((1 << required) - 1);
				break;
			}
		} while (required);
		return val;
	}

	uint32_t ScreenBuffer::fetchbits_signed(const uint8_t *p, uint32_t index, uint32_t required)
	{
		uint32_t val = fetchbits_unsigned(p, index, required);
		if (val & (1 << (required - 1)))
		{
			return (int32_t)val - (1 << required);
		}
		return (int32_t)val;
	}

	// Public methods ---------------------------------------------------------

	ScreenBuffer::ScreenBuffer(int width, int height, int x, int y)
	{
		bufferWidth = max(0, min(MAX_WIDTH, width));
		bufferHeight = max(0, min(MAX_HEIGHT, height));
		bufferX = max(0, min(MAX_WIDTH - bufferWidth, x));
		bufferY = max(0, min(MAX_HEIGHT - bufferHeight, y));

		this->buffer = new uint16_t[width * height];

		cursor_x = x;
		cursor_y = y;

		textcolor = COLOR(255, 255, 255);
	}

	ScreenBuffer::~ScreenBuffer()
	{
		delete[] this->buffer;
	}

	size_t ScreenBuffer::write(uint8_t c)
	{
		if (font == nullptr)
			return 0;

		if (c == '\n')
		{
			cursor_y += font->line_space; // Fix linefeed. Added by T.T., SoftEgg
			cursor_x = 0;
		}
		else
		{
			drawFontChar(c);
		}

		return 1;
	}

	void ScreenBuffer::drawHLine(int x1, int x2, int y, uint16_t color)
	{
		int px1 = max(bufferX, min(bufferX + bufferWidth - 1, min(x1, x2)));
		int px2 = max(bufferX, min(bufferX + bufferWidth - 1, max(x1, x2)));
		int py = max(bufferY, min(bufferY + bufferHeight - 1, y));
			
		for (int x = px1; x <= px2; x++)
		{
			buffer[(py - bufferY) * bufferWidth + (x - bufferX)] = color;
		}
	}

	void ScreenBuffer::drawVLine(int x, int y1, int y2, uint16_t color)
	{
		int px = max(bufferX, min(bufferX + bufferWidth - 1, x));
		int py1 = max(bufferY, min(bufferY + bufferHeight - 1, min(y1, y2)));
		int py2 = max(bufferY, min(bufferY + bufferHeight - 1, max(y1, y2)));

		for (int y = py1; y <= py2; y++)
		{
			buffer[(y - bufferY) * bufferWidth + (px - bufferX)] = color;
		}
	}

	void ScreenBuffer::drawLine(int x0, int y0, int x1, int y1, uint16_t color)
	{
		if (y0 == y1)
		{
			if (x1 > x0)
			{
				drawHLine(x0, y0, x1 - x0 + 1, color);
			}
			else if (x1 < x0)
			{
				drawHLine(x1, y0, x0 - x1 + 1, color);
			}
			else
			{
				buffer[y0 * bufferWidth + x0] = color;
			}
			return;
		}
		else if (x0 == x1)
		{
			if (y1 > y0)
			{
				drawVLine(x0, y0, y1 - y0 + 1, color);
			}
			else
			{
				drawVLine(x0, y1, y0 - y1 + 1, color);
			}
			return;
		}

		bool steep = abs(y1 - y0) > abs(x1 - x0);
		if (steep)
		{
			swap(x0, y0);
			swap(x1, y1);
		}
		if (x0 > x1)
		{
			swap(x0, x1);
			swap(y0, y1);
		}

		int dx, dy;
		dx = x1 - x0;
		dy = abs(y1 - y0);

		int err = dx / 2;
		int ystep;

		if (y0 < y1)
		{
			ystep = 1;
		}
		else
		{
			ystep = -1;
		}

		int xbegin = x0;
		if (steep)
		{
			for (; x0 <= x1; x0++)
			{
				err -= dy;
				if (err < 0)
				{
					int len = x0 - xbegin;
					if (len)
					{
						drawVLine(y0, xbegin, len + 1, color);
					}
					else
					{
						buffer[y0 * bufferWidth + x0] = color;
					}
					xbegin = x0 + 1;
					y0 += ystep;
					err += dx;
				}
			}
			if (x0 > xbegin + 1)
			{
				drawVLine(y0, xbegin, x0 - xbegin, color);
			}
		}
		else
		{
			for (; x0 <= x1; x0++)
			{
				err -= dy;
				if (err < 0)
				{
					int16_t len = x0 - xbegin;
					if (len)
					{
						drawHLine(xbegin, y0, len + 1, color);
					}
					else
					{
						buffer[y0 * bufferWidth + x0] = color;
					}
					xbegin = x0 + 1;
					y0 += ystep;
					err += dx;
				}
			}
			if (x0 > xbegin + 1)
			{
				drawHLine(xbegin, y0, x0 - xbegin, color);
			}
		}
	}

	void ScreenBuffer::fillRect(int x1, int y1, int x2, int y2, uint16_t color)
	{
		x1 = max(bufferX, min(bufferX + bufferWidth - 1, min(x1, x2)));
		x2 = max(bufferX, min(bufferX + bufferWidth - 1, max(x1, x2)));
		y1 = max(bufferY, min(bufferY + bufferHeight - 1, min(y1, y2)));
		y2 = max(bufferY, min(bufferY + bufferHeight - 1, max(y1, y2)));

		for (int y = y1; y <= y2; y++) 
		{
			for (int x = x1; x <= x2; x++) 
			{
				buffer[(y - bufferY) * bufferWidth + (x - bufferX)] = color;
			}
		}
	}

	void ScreenBuffer::drawRect(int x1, int y1, int x2, int y2, uint16_t color)
	{
		drawVLine(x1, y1, y2, color);
		drawVLine(x2, y1, y2, color);
		drawHLine(x1, x2, y1, color);
		drawHLine(x1, x2, y2, color);
	}

    void ScreenBuffer::drawPixel(int x, int y, uint16_t color)
	{
		buffer[y * bufferWidth + x] = color;
	}

	void ScreenBuffer::clear()
	{
		memset(buffer, 0, bufferWidth * bufferHeight * sizeof(uint16_t));
	}

	void ScreenBuffer::clear(uint16_t color)
	{
		uint16_t *ptr = buffer;

		for (int i = 0; i < bufferWidth * bufferHeight; i++)
			*ptr++ = color;
	}

	void ScreenBuffer::paintTo(Display *display)
	{
		display->writeRect(bufferX, bufferY, bufferWidth, bufferHeight, buffer);
	}

	// Public properties ------------------------------------------------------

	int ScreenBuffer::getWidth()
	{
		return bufferWidth;
	}

	int ScreenBuffer::getHeight()
	{
		return bufferHeight;
	}

	int ScreenBuffer::getX()
	{
		return bufferX;
	}

	int ScreenBuffer::getY()
	{
		return bufferY;
	}

	uint16_t *ScreenBuffer::getBuffer()
	{
		return buffer;
	}

	void ScreenBuffer::setFont(const ILI9341_t3_font_t &font)
	{
		this->font = &font;
	}

	void ScreenBuffer::setCursor(int curX, int curY)
	{
		cursor_x = max(bufferX, min(bufferX + bufferWidth - 1, curX));
		cursor_y = max(bufferY, min(bufferY + bufferHeight - 1, curY));
	}

	void ScreenBuffer::getCursor(int & curX, int & curY)
	{
		curX = cursor_x;
		curY = cursor_y;
	}

	void ScreenBuffer::setTextColor(uint16_t color)
	{
		this->textcolor = color;
	}

	void ScreenBuffer::printFloat(float f, int decimalPlaces) 
	{
        print((int)f);

        f -= (int)f;
        for (int i = 0; i < decimalPlaces; i++)
        f *= 10;

        print('.');

        print((int)f);
	}

} // namespace Multitool