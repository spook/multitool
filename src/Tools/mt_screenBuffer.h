#ifndef __MT_SCREEN_BUFFER_H__
#define __MT_SCREEN_BUFFER_H__

#include <ILI9341_t3.h>
#include "../Peripherials/mt_display.h"

#ifndef swap
#define swap(a, b) { typeof(a) t = a; a = b; b = t; }
#endif

namespace Multitool
{
    class ScreenBuffer : public Print
    {
    private:
        const int MAX_WIDTH = 320;
        const int MAX_HEIGHT = 240;

        uint16_t * buffer;
        int bufferWidth;
        int bufferHeight;
        int bufferX;
        int bufferY;

        const ILI9341_t3_font_t * font;
        uint16_t textcolor;
        bool wrap;
        int cursor_x;
        int cursor_y;

        void drawFontBits(uint32_t bits, uint32_t numbits, uint32_t x, uint32_t y, uint32_t repeat);
        void drawFontChar(unsigned int c);

        uint32_t fetchbit(const uint8_t *p, uint32_t index);
        uint32_t fetchbits_unsigned(const uint8_t *p, uint32_t index, uint32_t required);
        uint32_t fetchbits_signed(const uint8_t *p, uint32_t index, uint32_t required);

    public:
        ScreenBuffer(int width, int height, int x, int y);
        ~ScreenBuffer();

        size_t write(uint8_t c);
        void drawHLine(int x1, int x2, int y, uint16_t color);
        void drawVLine(int x, int y1, int y2, uint16_t color);
        void drawLine(int x1, int y1, int x2, int y2, uint16_t color);
        void fillRect(int x1, int y1, int x2, int y2, uint16_t color);
        void drawRect(int x1, int y1, int x2, int y2, uint16_t color);
        void drawPixel(int x, int y, uint16_t color);
        void clear();
        void clear(uint16_t color);
        void paintTo(Display * display);

        int getWidth();
        int getHeight();
        int getX();
        int getY();
        uint16_t * getBuffer();
        void setFont(const ILI9341_t3_font_t & font);
        void setCursor(int curX, int curY);
        void getCursor(int & curX, int & curY);
        void setTextColor(uint16_t color);        
        void printFloat(float f, int decimalPlaces);
    };
}

#endif