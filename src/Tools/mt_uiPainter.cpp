#include "mt_uiPainter.h"
#include "../Peripherials/mt_display.h"
#include "../Fonts/mt_crystal.h"
#include "../Fonts/mt_segoe.h"

namespace Multitool
{
    void UiPainter::internalPaintChart(
        ScreenBuffer * screenBuffer,
        DataSeries * dataSeries,
        float minValue,
        float maxValue,
        int x,
        int y,
        int width,
        int height,
        uint16_t chartColor,           
        bool useFillColor, 
        uint16_t chartFillColor)
    {
        int prevY = -1;
        for (int i = 0; i < width; i++) 
        {
            int index = (i * (dataSeries->size() - 1) / (width - 1));

            float value = (*dataSeries)[index];

            int dataX = x + i;
            int dataY = y + (height - 1) - (int)(((value - minValue) / (maxValue - minValue)) * (height - 1));

            if (useFillColor) 
            {
                screenBuffer->drawVLine(dataX, dataY, y + height - 1, chartFillColor);
            }

            if (i == 0)
            {
                screenBuffer->drawPixel(dataX, dataY, chartColor);
            }
            else if (dataY < y)
            {
                prevY = -1;
            }
            else if (dataY >= y + width)
            {
                prevY = 2147483647;
            }
            else
            {
                int validPrevY = prevY < y ? y : (prevY >= y + width ? y + width - 1 : prevY);

                if (dataY < validPrevY) 
                {
                    screenBuffer->drawVLine(dataX, validPrevY - 1, dataY, chartColor);
                }
                else if (dataY > validPrevY) 
                {
                    screenBuffer->drawVLine(dataX, validPrevY + 1, dataY, chartColor);
                }
                else
                {
                    screenBuffer->drawPixel(dataX, dataY, chartColor);
                }
            }

            prevY = dataY;
        }
    }

    void UiPainter::paintPanel(
        ScreenBuffer & screenBuffer,
        int x, 
        int y,
        int width,
        int height,
        const char * header,
        int headerWidth)
    {
        screenBuffer.drawHLine(x, x + width - 1, y, PANEL_LINE_COLOR);
        screenBuffer.drawVLine(x, y, y + height - 1, PANEL_LINE_COLOR);
        screenBuffer.drawHLine(x, x + width - 1, y + height - 1, PANEL_LINE_COLOR);
        screenBuffer.drawVLine(x + width - 1, y, y + height - 1, PANEL_LINE_COLOR);

        screenBuffer.fillRect(x, y, x + headerWidth, y + PANEL_HEADER_HEIGHT, PANEL_HEADER_COLOR);

        for (int i = x + headerWidth, j = PANEL_HEADER_HEIGHT; j > 0; i++, j--) 
        {
            screenBuffer.drawVLine(i, y, y + j, PANEL_HEADER_COLOR);
        }

        screenBuffer.setCursor(x + 3, y + 3);
        screenBuffer.setTextColor(PANEL_HEADER_TEXT_COLOR);
        screenBuffer.setFont(SegoeUi9);
        screenBuffer.print(header);
    }

    void UiPainter::paintShadowedText(
        ScreenBuffer & screenBuffer,
        float number, 
        int x, 
        int y,
        const ILI9341_t3_font_t & font)
    {
        screenBuffer.setFont(font);

        screenBuffer.setCursor(x+1, y+1);
        screenBuffer.setTextColor(COLOR(64, 64, 64));
        screenBuffer.printFloat(number, 1);

        screenBuffer.setCursor(x, y);
        screenBuffer.setTextColor(COLOR(255, 255, 255));
        screenBuffer.printFloat(number, 1);
    }

    void UiPainter::paintShadowedText(
        ScreenBuffer & screenBuffer,
        float number, 
        int precision,
        int x, 
        int y,
        const ILI9341_t3_font_t & font)
    {
        screenBuffer.setFont(font);

        screenBuffer.setCursor(x+1, y+1);
        screenBuffer.setTextColor(COLOR(64, 64, 64));
        screenBuffer.printFloat(number, precision);

        screenBuffer.setCursor(x, y);
        screenBuffer.setTextColor(COLOR(255, 255, 255));
        screenBuffer.printFloat(number, precision);
    }

    void UiPainter::paintShadowedText(
        ScreenBuffer & screenBuffer,
        int number, 
        int x, 
        int y,
        const ILI9341_t3_font_t & font)
    {
        screenBuffer.setFont(font);

        screenBuffer.setCursor(x+1, y+1);
        screenBuffer.setTextColor(COLOR(64, 64, 64));
        screenBuffer.print(number);

        screenBuffer.setCursor(x, y);
        screenBuffer.setTextColor(COLOR(255, 255, 255));
        screenBuffer.print(number);
    }

    void UiPainter::paintShadowedText(
        ScreenBuffer & screenBuffer,
        const char * text, 
        int x, 
        int y,
        const ILI9341_t3_font_t & font)
    {
        screenBuffer.setFont(font);

        screenBuffer.setCursor(x+1, y+1);
        screenBuffer.setTextColor(COLOR(64, 64, 64));
        screenBuffer.print(text);

        screenBuffer.setCursor(x, y);
        screenBuffer.setTextColor(COLOR(255, 255, 255));
        screenBuffer.print(text);
    }

    void UiPainter::paintMainButton(
            ScreenBuffer & screenBuffer,
            int x,
            int y,
            int width,
            int height)
    {
        screenBuffer.fillRect(x, y, x + width - 1, y + height - 1, COLOR(64, 64, 64));

        screenBuffer.drawHLine(x, x + width - 1, y, COLOR(255, 255, 255));
        screenBuffer.drawHLine(x, x + width - 1, y + height - 1, COLOR(255, 255, 255));

        screenBuffer.drawVLine(x, y, y + height - 1, COLOR(255, 255, 255));
        screenBuffer.drawVLine(x + width - 1, y, y + height - 1, COLOR(255, 255, 255));
    }

    void UiPainter::paintSingleChart(
        ScreenBuffer * screenBuffer,
        DataSeries * dataSeries,
        int x,
        int y,
        int width,
        int height,
        uint16_t chartColor,           
        bool useFillColor, 
        uint16_t chartFillColor)
    {
        internalPaintChart(screenBuffer,
            dataSeries,
            dataSeries->getMinValue(),
            dataSeries->getMaxValue(),
            x,
            y,
            width,
            height,
            chartColor,
            useFillColor,
            chartFillColor);
    }

    void UiPainter::paintDoubleCharts(
        ScreenBuffer * screenBuffer,
        DataSeries * firstSeries,
        DataSeries * secondSeries,
        int x,
        int y,
        int width,
        int height,
        uint16_t firstChartColor,
        uint16_t secondChartColor,
        uint16_t secondChartFillColor)
    {
        float minValue, maxValue;

        getChartsRanges(firstSeries, secondSeries, minValue, maxValue);

        internalPaintChart(screenBuffer,
            secondSeries,
            minValue,
            maxValue,
            x,
            y,
            width,
            height,
            secondChartColor,
            true,
            secondChartFillColor);

        internalPaintChart(screenBuffer,
            firstSeries,
            minValue,
            maxValue,
            x,
            y,
            width,
            height,
            firstChartColor,
            false,
            0);
    }

    void UiPainter::paintChartRanges(
        ScreenBuffer * screenBuffer,
        int x,
        int y,
        int width,
        int height,
        float minValue,
        float maxValue)
    {
        screenBuffer->setFont(SegoeUi9);
        screenBuffer->setTextColor(COLOR(255, 255, 255));

        screenBuffer->setCursor(x + width - 36, y + 3);
        screenBuffer->printFloat(maxValue, 1);
        screenBuffer->setCursor(x + width - 36, y + height - 8 - 3);
        screenBuffer->printFloat(minValue, 1);
    }

    void UiPainter::getChartsRanges(DataSeries * firstSeries,
        DataSeries * secondSeries,
        float & minValue,
        float & maxValue)
    {
        minValue = firstSeries->getMinValue() < secondSeries->getMinValue() ? firstSeries->getMinValue() : secondSeries->getMinValue();
        maxValue = firstSeries->getMaxValue() > secondSeries->getMaxValue() ? firstSeries->getMaxValue() : secondSeries->getMaxValue();

        if (maxValue - minValue < 1.0f) 
        {
            float mid = (minValue + maxValue) / 2.0f;
            minValue = mid - 0.5f;
            maxValue = mid + 0.5f;
        }
    }
}