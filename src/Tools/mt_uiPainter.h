#ifndef __MT_UI_PAINTER_H__
#define __MT_UI_PAINTER_H__

#include <ILI9341_t3.h>
#include "mt_screenBuffer.h"
#include "mt_dataSeries.h"

namespace Multitool 
{
    class UiPainter 
    {
    private:
        static void internalPaintChart(
            ScreenBuffer * screenBuffer,
            DataSeries * dataSeries,
            float minValue,
            float maxValue,
            int x,
            int y,
            int width,
            int height,
            uint16_t chartColor,           
            bool useFillColor, 
            uint16_t chartFillColor);        

    public:
        static const uint16_t PANEL_LINE_COLOR = COLOR(255, 255, 255);
        static const uint16_t PANEL_HEADER_COLOR = COLOR(255, 255, 255);
        static const uint16_t PANEL_HEADER_TEXT_COLOR = COLOR(0, 0, 0);
        static const int PANEL_HEADER_HEIGHT = 15;

        static void paintPanel(
            ScreenBuffer & screenBuffer,
            int x, 
            int y,
            int width,
            int height,
            const char * header,
            int headerWidth);

        static void paintShadowedText(
            ScreenBuffer & screenBuffer,
            float number, 
            int x, 
            int y,
            const ILI9341_t3_font_t & font);

        static void paintShadowedText(
            ScreenBuffer & screenBuffer,
            float number, 
            int precision,
            int x, 
            int y,
            const ILI9341_t3_font_t & font);

        static void paintShadowedText(
            ScreenBuffer & screenBuffer,
            int number, 
            int x, 
            int y,
            const ILI9341_t3_font_t & font);

        static void paintShadowedText(
            ScreenBuffer & screenBuffer,
            const char * text, 
            int x, 
            int y,
            const ILI9341_t3_font_t & font);

        static void paintMainButton(
            ScreenBuffer & screenBuffer,
            int x,
            int y,
            int width,
            int height);
    
        static void paintSingleChart(ScreenBuffer * screenBuffer,
            DataSeries * dataSeries,
            int x,
            int y,
            int width,
            int height,
            uint16_t chartColor,
            bool useFillColor,
            uint16_t chartFillColor);

        static void paintDoubleCharts(ScreenBuffer * screenBuffer,
            DataSeries * firstSeries,
            DataSeries * secondSeries,
            int x,
            int y,
            int width,
            int height,
            uint16_t firstChartColor,
            uint16_t secondChartColor,
            uint16_t secondChartFillColor);

        static void paintChartRanges(ScreenBuffer * screenBuffer,
            int x,
            int y,
            int width,
            int height,
            float minValue,
            float maxValue);

        static void getChartsRanges(DataSeries * data1,
            DataSeries * data2,
            float & minValue,
            float & maxValue);
    };
}

#endif